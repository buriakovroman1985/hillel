
n = int(input()) // 2
power, num = 0, 1
while num <= n:
    num <<= 1
    power += 1
print(f'{2} ** {power} = {num}')
